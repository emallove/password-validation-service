/**
 * PasswordValidatorServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wtp;

public class PasswordValidatorServiceLocator extends org.apache.axis.client.Service implements wtp.PasswordValidatorService {

    public PasswordValidatorServiceLocator() {
    }


    public PasswordValidatorServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PasswordValidatorServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PasswordValidator
    private java.lang.String PasswordValidator_address = "http://localhost:8080/PasswordValidatorService2/services/PasswordValidator";

    public java.lang.String getPasswordValidatorAddress() {
        return PasswordValidator_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PasswordValidatorWSDDServiceName = "PasswordValidator";

    public java.lang.String getPasswordValidatorWSDDServiceName() {
        return PasswordValidatorWSDDServiceName;
    }

    public void setPasswordValidatorWSDDServiceName(java.lang.String name) {
        PasswordValidatorWSDDServiceName = name;
    }

    public wtp.PasswordValidator getPasswordValidator() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PasswordValidator_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPasswordValidator(endpoint);
    }

    public wtp.PasswordValidator getPasswordValidator(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            wtp.PasswordValidatorSoapBindingStub _stub = new wtp.PasswordValidatorSoapBindingStub(portAddress, this);
            _stub.setPortName(getPasswordValidatorWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPasswordValidatorEndpointAddress(java.lang.String address) {
        PasswordValidator_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (wtp.PasswordValidator.class.isAssignableFrom(serviceEndpointInterface)) {
                wtp.PasswordValidatorSoapBindingStub _stub = new wtp.PasswordValidatorSoapBindingStub(new java.net.URL(PasswordValidator_address), this);
                _stub.setPortName(getPasswordValidatorWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PasswordValidator".equals(inputPortName)) {
            return getPasswordValidator();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://wtp", "PasswordValidatorService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://wtp", "PasswordValidator"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PasswordValidator".equals(portName)) {
            setPasswordValidatorEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
