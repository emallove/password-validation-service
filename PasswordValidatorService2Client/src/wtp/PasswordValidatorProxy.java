package wtp;

public class PasswordValidatorProxy implements wtp.PasswordValidator {
  private String _endpoint = null;
  private wtp.PasswordValidator passwordValidator = null;
  
  public PasswordValidatorProxy() {
    _initPasswordValidatorProxy();
  }
  
  public PasswordValidatorProxy(String endpoint) {
    _endpoint = endpoint;
    _initPasswordValidatorProxy();
  }
  
  private void _initPasswordValidatorProxy() {
    try {
      passwordValidator = (new wtp.PasswordValidatorServiceLocator()).getPasswordValidator();
      if (passwordValidator != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)passwordValidator)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)passwordValidator)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (passwordValidator != null)
      ((javax.xml.rpc.Stub)passwordValidator)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public wtp.PasswordValidator getPasswordValidator() {
    if (passwordValidator == null)
      _initPasswordValidatorProxy();
    return passwordValidator;
  }
  
  public boolean validate(java.lang.String password) throws java.rmi.RemoteException{
    if (passwordValidator == null)
      _initPasswordValidatorProxy();
    return passwordValidator.validate(password);
  }
  
  
}