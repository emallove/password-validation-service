package wtp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * 
 * Programming Problem
	Instructions:
	Write a password validation service, meant to be configurable via IoC (using dependency injection engine of your choice). 
	 The service is meant to check a text string for compliance to any number of password validation rules.
	   The rules currently known are:
	1.       Must consist of a mixture of lowercase letters and numerical digits only, with at least one of each.
	2.       Must be between 5 and 12 characters in length.
	3.       Must not contain any sequence of characters immediately followed by the same sequence.
	
	For any questions or clarifications, please contact
	
	Include all artifacts in a zip file and please let us know how many hours you spent on the programming problem.
	
     Effort: 2 hours
	
	 This project builds and runs using the following Eclipse version: 
	 
			Eclipse Standard/SDK
			Version: Kepler Service Release 1
			Build id: 20130919-0819
	 
	 A testing client is provided here:

	 	PasswordValidatorService2Client\WebContent\samplePasswordValidatorProxy
	 
	 The service is available at the following URL:
	 
	 	2Client/samplePasswordValidatorProxy/TestClient.jsp
	 
 * 
 * Sources:
 * 
 * 		http://www.eclipse.org/webtools/community/tutorials/BottomUpAxis2WebService/bu_tutorial.html
 * 		http://www.mkyong.com/regular-expressions/how-to-validate-password-with-regular-expression/
 */

public class PasswordValidator
{
	  
	  private Pattern pattern1;
	  private Pattern pattern2;

	  private Matcher matcher1;
	  private Matcher matcher2;
 
	  private static final String PASSWORD_PATTERN = 
			  "((?=.*\\d)(?=.*[a-z]).{5,12})";
	  
	  /* 
	   * Note: we only need to determine if there is a 
	   * two-character sequence that repeats
	   */
	  private static final String PASSWORD_PATTERN_SEQUENCE = 
			  "([a-z0-9]{2}).*\1";
 
	  public PasswordValidator(){
		  pattern1 = Pattern.compile(PASSWORD_PATTERN);
		  pattern2 = Pattern.compile(PASSWORD_PATTERN_SEQUENCE);
	  }
	  
 
	  public boolean validate ( String password )
	  {
		  /**
		   * Validate password with regular expression
		   * @param password password for validation
		   * @return true valid password, false invalid password
		   */
 
		  matcher1 = pattern1.matcher(password);
		  matcher2 = pattern2.matcher(password);

		  // 5-12 characters long with at least one lower-case and numeric character?
		  if (! matcher1.matches())
			  return false;

		  // Repeating pattern?
		  if (matcher2.matches())
			  return false;

		  return true;
	  }
}